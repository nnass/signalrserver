﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR.Hubs;
using System.Web.Security;
using System.Transactions;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;

namespace BasicChat.Mvc
{
    public class Authenticate : Hub
    {
        public Boolean Login(Models.LoginModel model)
        {
            if (WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            {
                return true;
            }
            else return false;
        }
    }
}